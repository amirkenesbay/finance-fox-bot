import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "3.2.1"
	id("io.spring.dependency-management") version "1.1.4"
	kotlin("jvm") version "1.9.21"
	kotlin("plugin.spring") version "1.9.21"
}

group = "com.financefox"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

sourceSets {
	main {
		java {
			srcDirs("src/main/kotlin")
		}
		kotlin {
			srcDirs("src/main/kotlin")
		}
	}
}

repositories {
	mavenLocal()
	mavenCentral()

	maven {
		url = uri("https://git.redmadrobot.com/api/v4/projects/2007/packages/maven")
		name = "GitLab Chat Machinist"

		credentials(HttpHeaderCredentials::class) {
			name = "Deploy-Token"
			value = "qX4MjySxV2Fe_kMzjx-J"
		}

		authentication {
			create("header", HttpHeaderAuthentication::class)
		}
	}

	maven {
		url = uri("https://git.redmadrobot.com/api/v4/projects/2039/packages/maven")
		name = "GitLab Chat Machinist Mongo"

		credentials(HttpHeaderCredentials::class) {
			name = "Deploy-Token"
			value = "j1c_bHFtgUdRyGZBfBnr"
		}

		authentication {
			create("header", HttpHeaderAuthentication::class)
		}
	}

}
val chatMachinistVersion: String by project
val chatMachinistMongoVersion: String by project

dependencies {
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("kz.rmr:chat-machinist:$chatMachinistVersion")
	implementation("kz.rmr:chat-machinist-mongo-persistence-starter:$chatMachinistMongoVersion")

	annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
