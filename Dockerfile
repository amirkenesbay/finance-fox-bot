FROM openjdk:17-jdk-slim

ENV JAVA_OPTS ""
WORKDIR /app
ARG JAR_FILE=build/libs/finance-fox-*.jar
COPY ${JAR_FILE} finance-fox.jar

CMD [ "sh", "-c", "java -jar -Dfile.encoding=UTF-8 ${JAVA_OPTS} /app/finance-fox.jar" ]
