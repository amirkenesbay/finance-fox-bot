package com.financefox.service

import com.financefox.domain.model.FinancialTransactionRequest

interface FinancialTransactionService {
    fun addFinancialTransaction(financialTransactionRequest: FinancialTransactionRequest)
}