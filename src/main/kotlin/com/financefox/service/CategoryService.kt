package com.financefox.service

import com.financefox.domain.model.Category
import com.financefox.domain.model.UserInfo

interface CategoryService {
    fun findAllCategories(userInfo: UserInfo): List<Category>
    fun findAllCategoriesWithMultipleUsers(userInfos: List<UserInfo>): List<Category>
    fun addCategory(categoryName: String, userInfo: UserInfo)
    fun updateCategory(oldCategoryName: String, newCategoryName: String)
    fun deleteCategory(categoryName: String)
}