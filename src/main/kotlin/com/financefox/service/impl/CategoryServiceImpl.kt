package com.financefox.service.impl

import com.financefox.domain.model.Category
import com.financefox.domain.model.UserInfo
import com.financefox.repository.CategoryRepository
import com.financefox.repository.entity.CategoryEntity
import com.financefox.repository.entity.UserInfoEntity
import com.financefox.service.CategoryService
import org.springframework.stereotype.Service

@Service
class CategoryServiceImpl(
    private val categoryRepository: CategoryRepository,
) : CategoryService {

    override fun findAllCategories(userInfo: UserInfo): List<Category> {
        val categories: List<CategoryEntity> = when {
            userInfo.username != null -> categoryRepository.findByUserUsername(userInfo.username)
            userInfo.telegramUserId != null -> categoryRepository.findByUserTelegramUserId(userInfo.telegramUserId)
            else -> emptyList()
        }

        return categories.map { mapEntity(it) }
    }

    override fun findAllCategoriesWithMultipleUsers(userInfos: List<UserInfo>): List<Category> {
        val categoriesSet = mutableSetOf<Category>()

        userInfos.forEach { userInfo ->
            val categories: List<CategoryEntity> = when {
                userInfo.username != null -> categoryRepository.findByUsername(userInfo.username)
                userInfo.telegramUserId != null -> categoryRepository.findByTelegramUserId(userInfo.telegramUserId)
                else -> emptyList()
            }

            categoriesSet.addAll(categories.map { mapEntity(it) })
        }

        return categoriesSet.toList()
    }

    override fun addCategory(categoryName: String, userInfo: UserInfo) {
        categoryRepository.save(mapModel(categoryName, userInfo))
    }

    override fun updateCategory(oldCategoryName: String, newCategoryName: String) {
        val categoryEntity = categoryRepository.findCategoryEntityByName(oldCategoryName)
            .orElseThrow { RuntimeException("Could not find category: $oldCategoryName") }

        categoryEntity.name = newCategoryName
        categoryRepository.save(categoryEntity)
    }

    override fun deleteCategory(categoryName: String) {
        categoryRepository.deleteCategoryEntityByName(categoryName)
    }

    private fun mapModel(categoryName: String, userInfo: UserInfo): CategoryEntity {
        return CategoryEntity(
            name = categoryName,
            user = UserInfoEntity(
                username = userInfo.username,
                firstName = userInfo.firstName,
                lastName = userInfo.lastName,
                telegramUserId = userInfo.telegramUserId,
                languageCode = userInfo.languageCode
            )
        )
    }

    private fun mapEntity(it: CategoryEntity): Category {
        return Category(
            id = it.id!!,
            name = it.name,
            username = it.user.username,
            telegramUserId = it.user.telegramUserId
        )
    }
}