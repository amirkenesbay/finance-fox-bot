package com.financefox.service.impl

import com.financefox.domain.model.NotificationPeriods.*
import com.financefox.domain.model.NotificationRequest
import com.financefox.repository.NotificationRepository
import com.financefox.repository.entity.NotificationEntity
import com.financefox.service.NotificationService
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.scheduling.TaskScheduler
import org.springframework.scheduling.support.CronTrigger
import org.springframework.stereotype.Service
import org.telegram.telegrambots.bots.DefaultAbsSender
import org.telegram.telegrambots.bots.DefaultBotOptions
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import java.time.LocalDateTime
import java.util.concurrent.ScheduledFuture

@Service
class NotificationServiceImpl(
    private val notificationRepository: NotificationRepository,
    private val taskScheduler: TaskScheduler,
    @Value("\${chat-machinist.bot.token}")
    private val botToken: String
) : NotificationService, DefaultAbsSender(
    DefaultBotOptions(), botToken
) {
    private var scheduledTasks: MutableMap<String, ScheduledFuture<*>?> = mutableMapOf()

    override fun addNotification(notificationRequest: NotificationRequest) {
        val notificationEntity = mapModel(notificationRequest)
        val savedEntity = notificationRepository.save(notificationEntity)
        rescheduleNotification(savedEntity)
    }

    override fun findAllNotifications(): List<NotificationRequest> {
        return notificationRepository.findAll().map {
            mapEntity(it)
        }
    }

    override fun enableNotification(notificationId: String, requesterChatId: Long) {
        try {
            val objectId = ObjectId(notificationId)
            val optionalNotification = notificationRepository.findById(objectId)

            if (optionalNotification.isPresent) {
                val notificationEntity = optionalNotification.get()

                // Проверяем, не активно ли уже уведомление
                if (!notificationEntity.isActive) {
                    notificationEntity.isActive = true
                    notificationRepository.save(notificationEntity)
                    println("Notification enabled for ID: $notificationId")

                    // Перепланируем уведомление
                    rescheduleNotification(notificationEntity)
                } else {
                    println("Notification already active for ID: $notificationId")
                }
            } else {
                println("Notification not found for ID: $notificationId")
            }
        } catch (e: IllegalArgumentException) {
            println("Invalid ObjectId: $notificationId")
        }
    }

    override fun disableNotification(notificationId: String, requesterChatId: Long) {
        scheduledTasks[notificationId]?.let { future ->
            if (!future.isCancelled) {
                future.cancel(false)
                println("Task cancelled for ID: $notificationId")
            }
            scheduledTasks.remove(notificationId)
            try {
                val objectId = ObjectId(notificationId)  // Преобразование String в ObjectId
                notificationRepository.findById(objectId).let {
                    it.get().isActive = false
                    notificationRepository.save(it.get())
                }
            } catch (e: IllegalArgumentException) {
                println("Invalid ObjectId: $notificationId")
            }
        } ?: println("No scheduled task found for ID: $notificationId")
    }

    private fun rescheduleNotification(savedEntity: NotificationEntity) {
        println("Rescheduling notification for ID: ${savedEntity.id}")

        val idString = savedEntity.id.toString()
        scheduledTasks[idString]?.cancel(false)

        val scheduledFuture = taskScheduler.schedule(
            { sendMessage(savedEntity.requesterChatId) },
            CronTrigger(createCronExpression(savedEntity))
        )
        scheduledTasks[idString] = scheduledFuture
        println("Scheduled task for ID: ${savedEntity.id}")
    }

    private fun sendMessage(requesterChatId: Long): Int {
        val sendMessage = SendMessage().apply {
            this.text = "Не забудь внести доходы/расходы /financial_operations"
            this.chatId = requesterChatId.toString()
        }
        return execute(sendMessage).messageId
    }

    private fun createCronExpression(entity: NotificationEntity): String {
        val dateTime = entity.dateTime!!
        return when (entity.period) {
            TEST.description -> "0/6 * * * * *"
            ONCE.description -> "0 ${dateTime.minute} ${dateTime.hour} ${dateTime.dayOfMonth} ${dateTime.monthValue} ?"
            DAILY.description -> "0 ${dateTime.minute} ${dateTime.hour} ? * *"
            WEEKLY.description -> "0 ${dateTime.minute} ${dateTime.hour} ? * ${dateTime.dayOfWeek.value}"
            BIWEEKLY.description -> "0 ${dateTime.minute} ${dateTime.hour} ? * ${dateTime.dayOfWeek.value}/2"
            MONTHLY.description -> "0 ${dateTime.minute} ${dateTime.hour} ${dateTime.dayOfMonth} * ?"
            BIMONTHLY.description -> "0 ${dateTime.minute} ${dateTime.hour} ${dateTime.dayOfMonth} 1/2 ?"
            QUARTERLY.description -> "0 ${dateTime.minute} ${dateTime.hour} ${dateTime.dayOfMonth} 1/3 ?"
            SEMIANNUALLY.description -> "0 ${dateTime.minute} ${dateTime.hour} ${dateTime.dayOfMonth} 1/6 ?"
            ANNUALLY.description -> "0 ${dateTime.minute} ${dateTime.hour} ${dateTime.dayOfMonth} 1 ?"
            else -> "0 ${dateTime.minute} ${dateTime.hour} ${dateTime.dayOfMonth} ${dateTime.monthValue} ? ${dateTime.year}"
        }
    }

    @EventListener(ApplicationReadyEvent::class)
    private fun initializeScheduledTasks() {
        notificationRepository.findAll().forEach { notification ->
            if (notification.isActive) {
                rescheduleNotification(notification)
            }
        }
    }

    private fun mapModel(notificationRequest: NotificationRequest): NotificationEntity {
        return NotificationEntity(
            id = null,
            name = notificationRequest.name.toString(),
            period = notificationRequest.period.toString(),
            dateTime = notificationRequest.dateTime!!,
            description = notificationRequest.description,
            requesterChatId = notificationRequest.requesterChatId!!,
            createdAt = LocalDateTime.now()
        )
    }

    private fun mapEntity(it: NotificationEntity): NotificationRequest {
        return NotificationRequest(
            id = it.id!!,
            name = it.name,
            period = it.period,
            dateTime = it.dateTime,
            description = it.description
        )
    }
}