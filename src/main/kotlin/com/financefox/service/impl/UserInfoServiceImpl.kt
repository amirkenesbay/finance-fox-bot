package com.financefox.service.impl

import com.financefox.domain.model.UserInfo
import com.financefox.repository.UserInfoRepository
import com.financefox.repository.entity.UserInfoEntity
import com.financefox.service.UserInfoService
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.objects.User

@Service
class UserInfoServiceImpl(
    private val userRepository: UserInfoRepository
) : UserInfoService {

    override fun getUserInfo(telegramUserInfo: User): UserInfo {
        val userInfo = findUser(telegramUserInfo)

        if (userInfo == null) {
            val entity = userRepository.save(mapModel(telegramUserInfo))
            return mapEntity(entity)
        }

        return mapEntity(userInfo)
    }

    private fun findUser(user: User): UserInfoEntity? {
        if (user.userName != null) {
            getByUsername(user.userName)
            return userRepository.findUserInfoEntityByUsername(user.userName)
        }

        return userRepository.findUserInfoEntityByTelegramUserId(user.id)
    }

    private fun getByUsername(username: String): UserInfoEntity? {
        return userRepository.findUserInfoEntityByUsername(username)
    }

    private fun mapModel(telegramUserInfo: User): UserInfoEntity {
        return UserInfoEntity(
            username = telegramUserInfo.userName,
            firstName = telegramUserInfo.firstName,
            lastName = telegramUserInfo.lastName,
            telegramUserId = telegramUserInfo.id,
            languageCode = telegramUserInfo.languageCode
        )
    }

    private fun mapEntity(it: UserInfoEntity): UserInfo {
        return UserInfo(
            id = it.id!!,
            username = it.username,
            firstName = it.firstName,
            lastName = it.lastName,
            telegramUserId = it.telegramUserId,
            languageCode = it.languageCode
        )
    }
}