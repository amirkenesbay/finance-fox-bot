package com.financefox.service.impl

import com.financefox.repository.AccountBalanceRepository
import com.financefox.repository.FinancialTransactionRepository
import com.financefox.repository.entity.AccountBalanceEntity
import com.financefox.service.BalanceService
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.objects.User
import java.math.BigDecimal
import java.time.LocalDateTime

@Service
class BalanceServiceImpl(
    private val transactionRepository: FinancialTransactionRepository,
    private val balanceRepository: AccountBalanceRepository
) : BalanceService {
    override fun existsAnyBalance(): Boolean {
        return balanceRepository.count() > 0
    }

    // TODO: make editing a balance
    override fun createBalance(balance: BigDecimal): BigDecimal {
        balanceRepository.save(mapModel(balance))
        return balance
    }

    override fun getCommonBalance(user: User): BigDecimal {
        val totals = transactionRepository.aggregateTotalAmountByType(user.id)
        var balance = BigDecimal.ZERO

        totals.forEach {
            when (it.getTransactionType()) {
                INCOME_OPERATION_TYPE -> balance = balance.add(it.getTotalAmount())
                EXPENSE_OPERATION_TYPE -> balance = balance.subtract(it.getTotalAmount())
            }
        }

        return balance
    }

    override fun getIncomeBalance(): BigDecimal {
        TODO("Not yet implemented")
    }

    override fun getExpenseBalance(): BigDecimal {
        TODO("Not yet implemented")
    }

    private fun mapModel(balance: BigDecimal): AccountBalanceEntity {
        return AccountBalanceEntity(
            balance = balance,
            // TODO: add real telegramUserId and username
            username = "asd",
            telegramUserId = 123L,
            createdAt = LocalDateTime.now(),
            modifiedAt = LocalDateTime.now()
        )
    }
}