package com.financefox.service.impl

import com.financefox.domain.model.Category
import com.financefox.domain.model.FinancialTransactionRequest
import com.financefox.repository.AccountBalanceRepository
import com.financefox.repository.FinancialTransactionRepository
import com.financefox.repository.entity.CategoryEntity
import com.financefox.repository.entity.FinancialTransactionEntity
import com.financefox.repository.entity.UserInfoEntity
import com.financefox.service.FinancialTransactionService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

const val INCOME_OPERATION_TYPE = "INCOME"
const val EXPENSE_OPERATION_TYPE = "EXPENSE"

@Service
class FinancialTransactionServiceImpl(
    private val repository: FinancialTransactionRepository,
    private val accountBalanceRepository: AccountBalanceRepository
) : FinancialTransactionService {

    @Transactional
    override fun addFinancialTransaction(financialTransactionRequest: FinancialTransactionRequest) {
        val financialTransactionEntity = mapModel(financialTransactionRequest)
        repository.save(financialTransactionEntity)
        updateBalance(financialTransactionEntity)
    }

    private fun mapModel(financialTransactionRequest: FinancialTransactionRequest): FinancialTransactionEntity {
        return FinancialTransactionEntity(
            id = null,
            amount = financialTransactionRequest.amount!!,
            description = financialTransactionRequest.description,
            transactionType = financialTransactionRequest.transactionType.toString(),
            date = financialTransactionRequest.date!!,
            categoryEntity = financialTransactionRequest.category?.let { mapCategoryModel(it) },
            user = UserInfoEntity(
                username = financialTransactionRequest.userInfo!!.username,
                firstName = financialTransactionRequest.userInfo.firstName,
                lastName = financialTransactionRequest.userInfo.lastName,
                telegramUserId = financialTransactionRequest.userInfo.telegramUserId,
                languageCode = financialTransactionRequest.userInfo.languageCode
            ),
            createdAt = LocalDateTime.now()
        )
    }

    private fun mapCategoryModel(category: Category): CategoryEntity {
        return CategoryEntity(
            id = category.id,
            name = category.name,
            user = UserInfoEntity(username = "FahrieOmerkaan", firstName = "Fahrie", lastName = "Omerkaan", telegramUserId = 502113794L, languageCode = "ru")
        )
    }

    private fun updateBalance(transaction: FinancialTransactionEntity) {
        val balanceEntity = accountBalanceRepository.findLatest()

        if (transaction.transactionType == INCOME_OPERATION_TYPE) {
            balanceEntity?.balance = balanceEntity?.balance?.add(transaction.amount)
        } else if (transaction.transactionType == EXPENSE_OPERATION_TYPE) {
            balanceEntity?.balance = balanceEntity?.balance?.subtract(transaction.amount)
        }

        balanceEntity?.let { accountBalanceRepository.save(it) }
    }
}