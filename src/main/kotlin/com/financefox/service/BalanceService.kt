package com.financefox.service

import org.telegram.telegrambots.meta.api.objects.User
import java.math.BigDecimal

interface BalanceService {
    fun existsAnyBalance(): Boolean
    fun createBalance(balance: BigDecimal) : BigDecimal
    fun getCommonBalance(user: User): BigDecimal
    fun getIncomeBalance(): BigDecimal
    fun getExpenseBalance(): BigDecimal
}