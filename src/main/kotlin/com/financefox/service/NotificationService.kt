package com.financefox.service

import com.financefox.domain.model.NotificationRequest

interface NotificationService {
    fun addNotification(notificationRequest: NotificationRequest)
    fun findAllNotifications(): List<NotificationRequest>
    fun enableNotification(notificationId: String, requesterChatId: Long)
    fun disableNotification(notificationId: String, requesterChatId: Long)
}