package com.financefox.service

import com.financefox.domain.model.UserInfo
import org.telegram.telegrambots.meta.api.objects.User

interface UserInfoService {
    fun getUserInfo(telegramUserInfo: User): UserInfo
}