package com.financefox.repository

interface Entity {
    var id: Long?
}