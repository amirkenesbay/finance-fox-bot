package com.financefox.repository.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime

@Document(collection = "financial_transactions")
data class FinancialTransactionEntity(
    @Id
    val id: ObjectId? = null,
    val amount: BigDecimal,
    val description: String?,
    val transactionType: String,
    val date: LocalDate,
    @DBRef
    val categoryEntity: CategoryEntity?,
    val user: UserInfoEntity,
    val createdAt: LocalDateTime
    // TODO: add modifiedAt
)
