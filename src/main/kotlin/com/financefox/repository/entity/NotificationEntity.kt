package com.financefox.repository.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "notifications")
data class NotificationEntity(
    @Id
    val id: ObjectId? = null,
    var name: String,
    var period: String,
    var dateTime: LocalDateTime,
    var description: String?,
    var isActive: Boolean = true,
    var requesterChatId: Long,
    @CreatedDate
    val createdAt: LocalDateTime,
    @LastModifiedDate
    var modifiedAt: LocalDateTime? = null
)