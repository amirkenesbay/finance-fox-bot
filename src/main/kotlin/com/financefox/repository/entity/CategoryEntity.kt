package com.financefox.repository.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "categories")
data class CategoryEntity(
    @Id
    val id: ObjectId? = null,
    var name: String,
    val user: UserInfoEntity
)