package com.financefox.repository.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDateTime

@Document(collection = "account_balances")
data class AccountBalanceEntity(
    @Id
    val accountId: ObjectId? = null,
    var balance: BigDecimal?,
    val username: String?,
    val telegramUserId: Long?,
    @CreatedDate
    val createdAt: LocalDateTime,
    @LastModifiedDate
    var modifiedAt: LocalDateTime? = null
)