package com.financefox.repository

import com.financefox.repository.entity.CategoryEntity
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface CategoryRepository : MongoRepository<CategoryEntity, ObjectId> {
    fun deleteCategoryEntityByName(name: String)
    fun findCategoryEntityByName(name: String): Optional<CategoryEntity>
    @Query("{ 'user.username': ?0 }")
    fun findByUsername(username: String): List<CategoryEntity>
    @Query("{ 'user.telegramUserId': ?0 }")
    fun findByTelegramUserId(telegramUserId: Long): List<CategoryEntity>
    fun findByUserUsername(username: String): List<CategoryEntity>
    fun findByUserTelegramUserId(telegramUserId: Long): List<CategoryEntity>
}