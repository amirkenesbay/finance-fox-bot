package com.financefox.repository

import com.financefox.repository.entity.AccountBalanceEntity
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface AccountBalanceRepository : MongoRepository<AccountBalanceEntity, ObjectId> {
    @Query(value = "{}", sort = "{ 'createdAt': -1 }")
    fun findLatest(): AccountBalanceEntity?
    fun findByUsername(username: String): List<AccountBalanceEntity>
    fun findByTelegramUserId(telegramUserId: Long): List<AccountBalanceEntity>
}