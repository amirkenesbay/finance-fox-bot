package com.financefox.repository

import com.financefox.repository.entity.FinancialTransactionEntity
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Field
import org.springframework.data.mongodb.repository.Aggregation
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.math.BigDecimal

@Repository
interface FinancialTransactionRepository : MongoRepository<FinancialTransactionEntity, ObjectId> {
    @Aggregation(pipeline = [
        "{ \$match: { amount: { \$exists: true, \$ne: null }, telegramUserId: ?0 } }", // Ensure the correct field is matched
        "{ \$addFields: { convertedAmount: { \$toDecimal: '\$amount' } } }",
        "{ \$group: { _id: '\$transactionType', totalAmount: { \$sum: '\$convertedAmount' } } }",
        "{ \$project: { transactionType: '\$_id', totalAmount: 1, _id: 0 } }"
    ])
    fun aggregateTotalAmountByType(telegramUserId: Long): List<AmountByTypeImpl>

    interface AmountByType {
        fun getTransactionType(): String
        fun getTotalAmount(): BigDecimal
    }
}

class AmountByTypeImpl(
    @Field("transactionType")
    private val transactionType: String?,
    @Field("totalAmount")
    private val totalAmount: BigDecimal?
) : FinancialTransactionRepository.AmountByType {
    override fun getTransactionType(): String = transactionType ?: "Unknown"
    override fun getTotalAmount(): BigDecimal = totalAmount ?: BigDecimal.ZERO
}
