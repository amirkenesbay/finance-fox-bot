package com.financefox.repository

interface Repository<T: Entity> {
    fun save(t: T): T
}