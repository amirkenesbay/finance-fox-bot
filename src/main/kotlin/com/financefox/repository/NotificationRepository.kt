package com.financefox.repository

import com.financefox.repository.entity.NotificationEntity
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface NotificationRepository : MongoRepository<NotificationEntity, ObjectId>