package com.financefox.util

import java.math.BigDecimal
import java.text.DecimalFormatSymbols
import java.util.*

private const val STRING_SPACE = " "
private const val EMPTY_STRING = ""
private const val STRING_DOT = "."

// TODO: что будет если ввести вместо числа буквы? Сделать доп транзишн который выводит на экран что введенное число не правильное
fun mapBigDecimal(amount: String): BigDecimal {
    val symbols = DecimalFormatSymbols(Locale.getDefault())
    val decimalSeparator = symbols.decimalSeparator
    val groupingSeparator = symbols.groupingSeparator

    val formattedAmount = amount
        .replace(STRING_SPACE, EMPTY_STRING)
        .replace(groupingSeparator.toString(), EMPTY_STRING)
        .replace(decimalSeparator.toString(), STRING_DOT)

    return BigDecimal(formattedAmount)
}