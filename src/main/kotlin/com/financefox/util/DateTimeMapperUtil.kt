package com.financefox.util

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

private const val TODAY_DATE = "Сегодня"
private const val YESTERDAY_DATE = "Вчера"
private const val DAY_BEFORE_YESTERDAY_DATE = "Позавчера"

/**
 * Converts a predefined string representing a relative date into a {@link LocalDate}.
 *
 * @param buttonText The string representing the date. Expected values are constants that may correspond
 * to "TODAY_DATE", "YESTERDAY_DATE", "DAY_BEFORE_YESTERDAY_DATE", or others.
 * @return The corresponding {@link LocalDate}. Returns today's date for unrecognized strings.
 */
fun mapStringDate(buttonText: String): LocalDate {
    return when (buttonText) {
        TODAY_DATE -> LocalDate.now()
        YESTERDAY_DATE -> LocalDate.now().minusDays(1)
        DAY_BEFORE_YESTERDAY_DATE -> LocalDate.now().minusDays(2)
        else -> LocalDate.now()
    }
}

/**
 * Attempts to parse a date string into a {@link LocalDate} based on several possible date formats.
 *
 * @param textDate The date string to be parsed.
 * @return A {@link LocalDate} object if the parsing is successful; null otherwise.
 * This function iterates over a predefined list of date formats and tries to parse the input string using
 * these formats. It returns the date as soon as a valid format is found or null if no valid format is identified.
 */
fun mapLocalDate(textDate: String): LocalDate? {
    val pattern = getDateFormatPatterns().find { pattern ->
        try {
            val formatter = DateTimeFormatter.ofPattern(pattern)
            LocalDate.parse(textDate, formatter)
            true
        } catch (e: DateTimeParseException) {
            false
        }
    }

    return pattern?.let {
        val formatter = DateTimeFormatter.ofPattern(it)
        LocalDate.parse(textDate, formatter)
    }
}

/**
 * Attempts to parse a datetime string into a {@link LocalDateTime} based on several possible datetime formats.
 *
 * @param textDateTime The datetime string to be parsed.
 * @return A {@link LocalDateTime} object if the parsing is successful; null otherwise.
 * This function first normalizes the input by trimming and reducing multiple spaces to a single space.
 * It then iterates through a list of predefined datetime formats, trying to parse the cleaned string.
 * The function returns the datetime as soon as a valid format is found or null if no valid format is identified.
 */
fun mapLocalDateTime(textDateTime: String): LocalDateTime? {
    val cleanedDateTime = textDateTime.trim().replace(Regex("\\s+"), " ")
    val dateTimePatterns = getDateTimeFormatPatterns()

    val pattern = dateTimePatterns.find { pattern ->
        try {
            val formatter = DateTimeFormatter.ofPattern(pattern)
            LocalDateTime.parse(cleanedDateTime, formatter)
            true
        } catch (e: DateTimeParseException) {
            false
        }
    }

    return pattern?.let {
        val formatter = DateTimeFormatter.ofPattern(it)
        LocalDateTime.parse(cleanedDateTime, formatter)
    }
}

fun getDateFormatPatterns(): List<String> {
    return listOf(
        "yyyy.MM.dd",
        "MM.dd.yyyy",
        "yyyy-MM-dd",
        "yyyy/MM/dd",
        "yyyy-M-d",
        "yyyy/M/d",
        "yyyy.dd.MM",
        "d.M.yyyy",
        "d-M-yyyy",
        "d/M/yyyy",
        "dd.MM.yyyy",
        "dd-MM-yyyy",
        "dd/MM/yyyy",
        "M/d/yyyy"
    )
}

fun getDateTimeFormatPatterns(): List<String> {
    return listOf(
        "dd/MM/yyyy HH:mm",
        "MM/dd/yyyy HH:mm",
        "yyyy-MM-dd HH:mm",
        "yyyy/MM/dd HH:mm",
        "d/M/yyyy HH:mm",
        "M/d/yyyy HH:mm"
    )
}