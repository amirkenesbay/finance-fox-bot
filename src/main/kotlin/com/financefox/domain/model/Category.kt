package com.financefox.domain.model

import org.bson.types.ObjectId

data class Category(
    var id: ObjectId,
    var name: String,
    val username: String?,
    val telegramUserId: Long?
)