package com.financefox.domain.model

import org.bson.types.ObjectId

data class UserInfo(
    val id: ObjectId? = null,
    val username: String?,
    val firstName: String?,
    val lastName: String?,
    val telegramUserId: Long?,
    val languageCode: String?
)
