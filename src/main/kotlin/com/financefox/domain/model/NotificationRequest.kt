package com.financefox.domain.model

import org.bson.types.ObjectId
import java.time.LocalDateTime

data class NotificationRequest(
    val id: ObjectId? = null,
    var name: String? = null,
    var period: String? = null,
    var dateTime: LocalDateTime? = null,
    var description: String? = null,
    var requesterChatId: Long? = null
)