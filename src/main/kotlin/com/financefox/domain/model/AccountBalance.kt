package com.financefox.domain.model

import org.bson.types.ObjectId
import java.math.BigDecimal

data class AccountBalance(
    val accountId: ObjectId? = null,
    var balance: BigDecimal?,
    val username: String?,
    val telegramUserId: Long?,
)
