package com.financefox.domain.model

import org.bson.types.ObjectId
import java.math.BigDecimal
import java.time.LocalDate

data class FinancialTransactionRequest(
    val id: ObjectId?=null,
    var amount: BigDecimal?=null,
    var description: String?=null,
    var transactionType: TransactionType?=null,
    var date: LocalDate?=null,
    var category: Category?=null,
    val userInfo: UserInfo?=null
)

enum class TransactionType {
    INCOME,
    EXPENSE
}