package com.financefox.domain.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.math.BigDecimal

@JsonFormat(shape = JsonFormat.Shape.BINARY)
enum class FinanceFoxState {
    // -------- MENU --------
    STARTED,
    MENU,

    // -------- NOTIFICATION --------
    NOTIFICATION_CHOICES,
    NOTIFICATION_SELECTION,
    TYPE_NOTIFICATION_NAME,
    DISABLING_NOTIFICATION,
    ENABLING_NOTIFICATION,
    SELECT_NOTIFICATION_PERIODICITY,
    TYPE_NOTIFICATION_DATE_TIME,
    TYPE_NOTIFICATION_DESCRIPTION,
    APPLY_NOTIFICATION,

    // -------- CATEGORY --------
    CATEGORY_CHOICES,
    ADDING_CATEGORY,
    CATEGORY_ADDED,
    CATEGORY_SELECTION,
    DELETING_CATEGORY,
    UPDATING_CATEGORY,
    CATEGORY_UPDATED,

    // -------- FINANCIAL OPERATION --------
    START_FINANCIAL_OPERATION,
    SELECT_CATEGORY,
    SELECT_DATE,
    TYPE_OTHER_DATE,
    TYPE_AMOUNT,
    TYPE_DESCRIPTION,
    APPLY_FINANCIAL_OPERATION,

    // -------- BALANCE --------
    BALANCE_CHOICES,
    EDITING_BALANCE,
    BALANCE_ADDED,

}

enum class FinanceFoxButtonType {
    // -------- CURRENCY --------
    CURRENCY,

    // -------- NOTIFICATION --------
    NOTIFICATIONS,
    ADD_NOTIFICATION,
    UPDATE_NOTIFICATION,
    DELETE_NOTIFICATION,
    DISABLE_NOTIFICATION,
    ENABLE_NOTIFICATION,
    NOTIFICATION_PERIODICITY,
    SKIP_TYPING_NOTIFICATION_DESCRIPTION,

    // -------- CATEGORY --------
    CATEGORIES,
    ADD_CATEGORY,
    UPDATE_CATEGORY,
    DELETE_CATEGORY,

    // <<<- GO BACK CATEGORY --
    GO_BACK_TO_MENU,
    GO_TO_CATEGORIES,
    GO_TO_ADDING_CATEGORY,
    GO_TO_CATEGORY_CHOICES,

    // -------- FINANCIAL OPERATION --------
    INCOME,
    EXPENSE,
    FINANCIAL_OPERATION_DATE,
    FINANCIAL_OPERATION_OTHER_DATE,
    SKIP_TYPING_DESCRIPTION,

    // <<<- GO BACK FINANCIAL OPERATION --
    GO_BACK_TO_FINANCIAL_OPERATIONS,
    GO_BACK_TO_SELECTING_CATEGORY,
    GO_BACK_TO_SELECTING_DATE,
    GO_BACK_TO_TYPING_OTHER_DATE,
    GO_BACK_TO_TYPING_AMOUNT,

    // -------- BALANCE --------
    BALANCE,
    CREATE_BALANCE,
}

class FinanceFoxContext {
    // -------- USER --------
    var userInfo: UserInfo? = null

    // -------- NOTIFICATION --------
    var notificationRequest: NotificationRequest? = null
    var allNotifications: List<NotificationRequest> = emptyList()
    var chosenNotification: NotificationRequest? = null

    // -------- CATEGORY --------
    var allCategories: List<Category> = emptyList()
    var chosenCategory: Category? = null

    // -------- FINANCIAL OPERATION --------
    var financialTransactionRequest: FinancialTransactionRequest? = null
    var category: Category? = null
    var isInvalidDate: Boolean? = false
    var isChosenOtherDate: Boolean? = false
    var isSkippedTypingDescriptionForFinance: Boolean? = false

    // -------- FINANCIAL OPERATION --------
    var balance: BigDecimal? = null
    var isBalanceCreated: Boolean? = null
}