package com.financefox.domain.model

enum class NotificationPeriods(val description: String) {
    TEST("Тест"),
    ONCE("Один раз"),
    DAILY("Каждый день"),
    WEEKLY("Каждую неделю"),
    BIWEEKLY("Каждые 2 недели"),
    MONTHLY("Каждый месяц"),
    BIMONTHLY("Каждые 2 месяца"),
    QUARTERLY("Каждый квартал"),
    SEMIANNUALLY("Каждые полгода"),
    ANNUALLY("Каждый год");

    companion object {
        fun periodicityList() = entries.map { it.description }
    }
}