package com.financefox.chat.financefox.dialog

import com.financefox.chat.financefox.transition.balance.balanceTransitions
import com.financefox.chat.financefox.transition.category.categoryTransitions
import com.financefox.chat.financefox.transition.notification.notificationTransitions
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.BalanceService
import com.financefox.service.CategoryService
import com.financefox.service.NotificationService
import com.financefox.service.UserInfoService
import kz.rmr.chatmachinist.api.transition.ChatBuilder
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun ChatBuilder<FinanceFoxState, FinanceFoxContext>.financeFoxDialog(
    categoryService: CategoryService,
    balanceService: BalanceService,
    notificationService: NotificationService,
    userInfoService: UserInfoService
) {
    dialog {
        name = "Finance fox dialog"

        startFinanceFoxDialogTransition(userInfoService)
        notificationTransitions(notificationService)
        balanceTransitions(balanceService)
        categoryTransitions(categoryService)
    }
}

private fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.startFinanceFoxDialogTransition(
    userInfoService: UserInfoService
) {
    transition {
        name = "Start Finance Fox Dialog"
        startDialog = true

        condition {
            eventType = EventType.COMMAND
            text = "/start"
        }

        action {
            context.userInfo = userInfoService.getUserInfo(user)
        }

        then {
            to = FinanceFoxState.STARTED

            noReply = true

            trigger {
                sameDialog = true
            }
        }
    }

    transition {
        name = "Open menu"

        condition {
            from = FinanceFoxState.STARTED
            eventType = EventType.TRIGGERED
        }

        then {
            to = FinanceFoxState.MENU
        }
    }
}