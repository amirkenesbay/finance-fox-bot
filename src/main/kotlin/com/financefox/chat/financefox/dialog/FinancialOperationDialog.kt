package com.financefox.chat.financefox.dialog

import com.financefox.chat.financefox.transition.finance.financialOperationsTransitions
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.domain.model.FinancialTransactionRequest
import com.financefox.service.CategoryService
import com.financefox.service.FinancialTransactionService
import com.financefox.service.UserInfoService
import kz.rmr.chatmachinist.api.transition.ChatBuilder
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun ChatBuilder<FinanceFoxState, FinanceFoxContext>.financialOperationDialog(
    categoryService: CategoryService,
    financialTransactionService: FinancialTransactionService,
    userInfoService: UserInfoService
) {
    dialog {
        name = "Financial operation dialog"
        startFinancialOperationDialogTransition(userInfoService)
        financialOperationsTransitions(categoryService, financialTransactionService)
    }
}

private fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.startFinancialOperationDialogTransition(
    userInfoService: UserInfoService
) {
    transition {
        name = "Start financial operation dialog"
        startDialog = true

        condition {
            eventType = EventType.COMMAND
            text = "/financial_operations"
        }

        action {
            context.userInfo = userInfoService.getUserInfo(user)
            println("Teeest " + context.userInfo)
            context.financialTransactionRequest = FinancialTransactionRequest(
                userInfo = context.userInfo
            )
        }

        then {
            to = FinanceFoxState.START_FINANCIAL_OPERATION
        }
    }
}