package com.financefox.chat.financefox.transition.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.FinancialTransactionService
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.typeDescriptionForAmount(
    financialTransactionService: FinancialTransactionService
) {
    transition {
        name = "Typing description"

        condition {
            from = FinanceFoxState.TYPE_DESCRIPTION
            eventType = EventType.TEXT
        }

        action {
            context.financialTransactionRequest?.description = text
            context.financialTransactionRequest?.let { financialTransactionService.addFinancialTransaction(it) }
        }

        then {
            to = FinanceFoxState.APPLY_FINANCIAL_OPERATION
        }
    }

    transition {
        name = "Skip typing description"

        condition {
            from = FinanceFoxState.TYPE_DESCRIPTION
            button = FinanceFoxButtonType.SKIP_TYPING_DESCRIPTION
        }

        action {
            context.isSkippedTypingDescriptionForFinance = true
            context.financialTransactionRequest?.let { financialTransactionService.addFinancialTransaction(it) }
        }

        then {
            to = FinanceFoxState.APPLY_FINANCIAL_OPERATION
        }
    }

    transition {
        name = "Go back from typing description to selecting date"

        condition {
            from = FinanceFoxState.TYPE_DESCRIPTION
            button = FinanceFoxButtonType.GO_BACK_TO_TYPING_AMOUNT
        }

        then {
            to = FinanceFoxState.TYPE_AMOUNT
        }
    }
}