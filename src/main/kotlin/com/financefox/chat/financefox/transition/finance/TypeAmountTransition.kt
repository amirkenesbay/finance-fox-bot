package com.financefox.chat.financefox.transition.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.util.mapBigDecimal
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.typeAmount() {
    transition {
        name = "Typing amount"

        condition {
            from = FinanceFoxState.TYPE_AMOUNT
            eventType = EventType.TEXT
        }

        action {
            context.financialTransactionRequest?.amount = text?.let { mapBigDecimal(it) }
        }

        then {
            to = FinanceFoxState.TYPE_DESCRIPTION
        }
    }

    transition {
        name = "Go back from invalid data to selecting other date"

        condition {
            from = FinanceFoxState.TYPE_AMOUNT
            button = FinanceFoxButtonType.GO_BACK_TO_TYPING_OTHER_DATE
        }

        action {
            context.isChosenOtherDate = false
            context.isInvalidDate = false
        }

        then {
            to = FinanceFoxState.TYPE_OTHER_DATE
        }
    }

    transition {
        name = "Go back from selecting amount to selecting date"

        condition {
            from = FinanceFoxState.TYPE_AMOUNT
            button = FinanceFoxButtonType.GO_BACK_TO_SELECTING_DATE
        }

        then {
            to = FinanceFoxState.SELECT_DATE
        }
    }
}