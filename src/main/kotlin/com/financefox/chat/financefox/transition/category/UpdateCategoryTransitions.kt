package com.financefox.chat.financefox.transition.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.CategoryService
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.updateCategory(categoryService: CategoryService) {
    transition {
        name = "Updating the category"

        condition {
            from = FinanceFoxState.CATEGORY_SELECTION
            button = FinanceFoxButtonType.UPDATE_CATEGORY
        }

//        action {
//            context.chosenCategory?.name?.let { categoryService.updateCategory(it) }
//        }

        then {
            to = FinanceFoxState.UPDATING_CATEGORY
        }
    }

    transition {
        name = "Category updated"

        condition {
            from = FinanceFoxState.UPDATING_CATEGORY
            eventType = EventType.TEXT
        }

        action {
            val newCategoryName = text!!
            context.chosenCategory?.name?.let { categoryService.updateCategory(it, newCategoryName) }
        }

        then {
            to = FinanceFoxState.CATEGORY_UPDATED
        }
    }

//    transition {
//        name = "Go back from category options to menu"
//
//        condition {
//            from = FinanceFoxState.CATEGORY_SELECTION
//            button = FinanceFoxButtonType.GO_TO_CATEGORIES
//        }
//
//        then {
//            to = FinanceFoxState.CATEGORY_CHOICES
//        }
//    }
}