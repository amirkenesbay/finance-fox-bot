package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.enableNotification(notificationService: NotificationService) {
    transition {
        name = "Enable a notification"

        condition {
            from = FinanceFoxState.NOTIFICATION_SELECTION
            button = FinanceFoxButtonType.ENABLE_NOTIFICATION
        }

        action {
            notificationService.enableNotification(context.chosenNotification!!.id.toString(), chat.id)
        }

        then {
            to = FinanceFoxState.ENABLING_NOTIFICATION
        }
    }
}