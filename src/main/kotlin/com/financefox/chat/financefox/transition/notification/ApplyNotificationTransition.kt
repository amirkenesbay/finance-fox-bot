package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.applyNotification(
    notificationService: NotificationService
) {
    transition {
        name = "Apply notification"

        condition {
            from = FinanceFoxState.APPLY_NOTIFICATION
            button = FinanceFoxButtonType.GO_BACK_TO_MENU
        }

        then {
            to = FinanceFoxState.MENU
        }
    }
}