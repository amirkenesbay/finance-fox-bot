package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.typeNotificationDescription(
    notificationService: NotificationService
) {
    transition {
        name = "Type notification description"

        condition {
            from = FinanceFoxState.TYPE_NOTIFICATION_DESCRIPTION
            eventType = EventType.TEXT
        }

        action {
            context.notificationRequest?.description = text
            context.notificationRequest?.let { notificationService.addNotification(it) }
        }

        then {
            to = FinanceFoxState.APPLY_NOTIFICATION
        }
    }

    transition {
        name = "Skip typing notification description"

        condition {
            from = FinanceFoxState.TYPE_NOTIFICATION_DESCRIPTION
            button = FinanceFoxButtonType.SKIP_TYPING_NOTIFICATION_DESCRIPTION
        }

        action {
            context.notificationRequest?.let { notificationService.addNotification(it) }
        }

        then {
            to = FinanceFoxState.APPLY_NOTIFICATION
        }
    }
}