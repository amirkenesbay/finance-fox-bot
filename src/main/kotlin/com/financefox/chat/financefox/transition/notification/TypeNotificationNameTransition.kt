package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.domain.model.NotificationRequest
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.typeNotificationName() {
    transition {
        name = "Typing notification name"

        condition {
            from = FinanceFoxState.NOTIFICATION_CHOICES
            button = FinanceFoxButtonType.ADD_NOTIFICATION
        }

        action {
            context.notificationRequest = NotificationRequest()
        }

        then {
            to = FinanceFoxState.TYPE_NOTIFICATION_NAME
        }
    }

    transition {
        name = "Added notification name"

        condition {
            from = FinanceFoxState.TYPE_NOTIFICATION_NAME
            eventType = EventType.TEXT
        }

        action {
            context.notificationRequest?.name = text!!
            context.notificationRequest?.requesterChatId = chat.id
        }

        then {
            to = FinanceFoxState.SELECT_NOTIFICATION_PERIODICITY
        }
    }
}