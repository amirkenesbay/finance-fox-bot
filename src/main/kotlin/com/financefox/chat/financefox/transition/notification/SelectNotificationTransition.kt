package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.selectNotification() {
    transition {
        name = "Select notification"

        condition {
            from = FinanceFoxState.NOTIFICATION_CHOICES
            button = FinanceFoxButtonType.NOTIFICATIONS
        }

        action {
            context.chosenNotification = context.allNotifications
                .find {
                    it.name == buttonText
                }
        }

        then {
            to = FinanceFoxState.NOTIFICATION_SELECTION
        }
    }
}