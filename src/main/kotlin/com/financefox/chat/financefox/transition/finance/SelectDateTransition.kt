package com.financefox.chat.financefox.transition.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.util.mapLocalDate
import com.financefox.util.mapStringDate
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.selectDate() {
    transition {
        name = "Selecting date"

        condition {
            from = FinanceFoxState.SELECT_DATE
            button = FinanceFoxButtonType.FINANCIAL_OPERATION_DATE
        }

        action {
            context.isChosenOtherDate = false
            context.financialTransactionRequest?.date = buttonText?.let { mapStringDate(it) }
        }

        then {
            to = FinanceFoxState.TYPE_AMOUNT
        }
    }

    transition {
        name = "Selecting other date"

        condition {
            from = FinanceFoxState.SELECT_DATE
            button = FinanceFoxButtonType.FINANCIAL_OPERATION_OTHER_DATE
        }

        then {
            to = FinanceFoxState.TYPE_OTHER_DATE
        }
    }

    transition {
        name = "Typing other date for income"

        condition {
            from = FinanceFoxState.TYPE_OTHER_DATE
            eventType = EventType.TEXT
        }

        action {
            val date = text?.let { mapLocalDate(it) }
            context.financialTransactionRequest?.date = date
            context.isChosenOtherDate = date != null
            context.isInvalidDate = date == null
        }

        then {
            to = FinanceFoxState.TYPE_AMOUNT
        }
    }

    transition {
        name = "Go back from selecting date to selecting category for income"

        condition {
            from = FinanceFoxState.SELECT_DATE
            button = FinanceFoxButtonType.GO_BACK_TO_SELECTING_CATEGORY
        }

        then {
            to = FinanceFoxState.SELECT_CATEGORY
        }
    }

    transition {
        name = "Go back from typing other date to selecting date for income"

        condition {
            from = FinanceFoxState.TYPE_OTHER_DATE
            button = FinanceFoxButtonType.GO_BACK_TO_SELECTING_DATE
        }

        then {
            to = FinanceFoxState.SELECT_DATE
        }
    }
}