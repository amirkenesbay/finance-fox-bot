package com.financefox.chat.financefox.transition.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
//import com.financefox.service.CategoryService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.selectCategory() {
    transition {
        name = "Select category"

        condition {
            from = FinanceFoxState.CATEGORY_CHOICES
            button = FinanceFoxButtonType.CATEGORIES
        }

        action {
            context.chosenCategory = context.allCategories
                .find {
                    it.name == buttonText
                }
        }

        then {
            to = FinanceFoxState.CATEGORY_SELECTION
        }
    }

    transition {
        name = "Go back from category options to menu"

        condition {
            from = FinanceFoxState.CATEGORY_SELECTION
            button = FinanceFoxButtonType.GO_TO_CATEGORIES
        }

        then {
            to = FinanceFoxState.CATEGORY_CHOICES
        }
    }
}