package com.financefox.chat.financefox.transition.balance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.BalanceService
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.showBalance(balanceService: BalanceService) {
    transition {
        name = "Show balance"

        condition {
            from = FinanceFoxState.MENU
            button = FinanceFoxButtonType.BALANCE
        }

        action {
            context.balance = balanceService.getCommonBalance(user)
        }

        then {
            to = FinanceFoxState.BALANCE_CHOICES
        }
    }

    transition {
        name = "Go back from balance choices to menu"

        condition {
            from = FinanceFoxState.BALANCE_CHOICES
            button = FinanceFoxButtonType.GO_BACK_TO_MENU
        }

        then {
            to = FinanceFoxState.MENU
        }
    }
}