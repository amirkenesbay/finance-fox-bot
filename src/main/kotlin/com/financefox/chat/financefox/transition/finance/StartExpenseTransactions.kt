package com.financefox.chat.financefox.transition.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.domain.model.TransactionType
import com.financefox.service.CategoryService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.startExpenseTransition(categoryService: CategoryService) {
    transition {
        name = "Start adding the expense"

        condition {
            from = FinanceFoxState.START_FINANCIAL_OPERATION
            button = FinanceFoxButtonType.EXPENSE
        }

        action {
            context.allCategories = categoryService.findAllCategories(context.userInfo!!)
            context.financialTransactionRequest!!.transactionType = TransactionType.EXPENSE
        }

        then {
            to = FinanceFoxState.SELECT_CATEGORY
        }
    }
}