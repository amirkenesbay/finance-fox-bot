package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.util.mapLocalDateTime
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.typeNotificationDateTime() {
    transition {
        name = "Typing notification date time"

        condition {
            from = FinanceFoxState.TYPE_NOTIFICATION_DATE_TIME
            eventType = EventType.TEXT
        }

        action {
            context.notificationRequest?.dateTime = text?.let { mapLocalDateTime(it) }
        }

        then {
            to = FinanceFoxState.TYPE_NOTIFICATION_DESCRIPTION
        }
    }
}