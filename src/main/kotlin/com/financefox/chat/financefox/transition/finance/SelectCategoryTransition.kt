package com.financefox.chat.financefox.transition.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.selectCategory() {
    transition {
        name = "Selecting category"

        condition {
            from = FinanceFoxState.SELECT_CATEGORY
            button = FinanceFoxButtonType.CATEGORIES
        }

        action {
            context.chosenCategory = context.allCategories
                .find {
                    it.name == buttonText
                }
        }

        then {
            to = FinanceFoxState.SELECT_DATE
        }
    }

    transition {
        name = "Go back from selecting category to financial operations"

        condition {
            from = FinanceFoxState.SELECT_CATEGORY
            button = FinanceFoxButtonType.GO_BACK_TO_FINANCIAL_OPERATIONS
        }

        then {
            to = FinanceFoxState.START_FINANCIAL_OPERATION
        }
    }
}