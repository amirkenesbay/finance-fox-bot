package com.financefox.chat.financefox.transition.category

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.CategoryService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.categoryTransitions(categoryService: CategoryService) {
    showCategories(categoryService)
    addCategory(categoryService)
    selectCategory()
    deleteCategory(categoryService)
    updateCategory(categoryService)
}