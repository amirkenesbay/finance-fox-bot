package com.financefox.chat.financefox.transition.balance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.BalanceService
import com.financefox.util.mapBigDecimal
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.editBalance(balanceService: BalanceService) {
    transition {
        name = "Editing a balance"

        condition {
            from = FinanceFoxState.BALANCE_CHOICES
            button = FinanceFoxButtonType.BALANCE
        }

        then {
            to = FinanceFoxState.EDITING_BALANCE
        }
    }

    transition {
        name = "Balance edited"

        condition {
            from = FinanceFoxState.EDITING_BALANCE
            eventType = EventType.TEXT
        }

        action {
            val balanceAmount = mapBigDecimal(text!!)
            context.balance = balanceService.createBalance(balanceAmount)
        }

        then {
            to = FinanceFoxState.BALANCE_CHOICES
        }
    }

    transition {
        name = "Go back from balance choices to menu"

        condition {
            from = FinanceFoxState.EDITING_BALANCE
            button = FinanceFoxButtonType.GO_BACK_TO_MENU
        }

        then {
            to = FinanceFoxState.MENU
        }
    }
}