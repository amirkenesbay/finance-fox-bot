package com.financefox.chat.financefox.transition.finance

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.CategoryService
import com.financefox.service.FinancialTransactionService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.financialOperationsTransitions(
    categoryService: CategoryService,
    financialTransactionService: FinancialTransactionService
) {
    startAddingIncome(categoryService)
    // TODO: below rename the method and kotlin file
    startExpenseTransition(categoryService)
    selectCategory()
    selectDate()
    typeAmount()
    typeDescriptionForAmount(financialTransactionService)
    applyFinancialOperation()
}