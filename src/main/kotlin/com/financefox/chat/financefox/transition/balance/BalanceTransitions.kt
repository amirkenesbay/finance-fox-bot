package com.financefox.chat.financefox.transition.balance

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.BalanceService
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.balanceTransitions(balanceService: BalanceService) {
    showBalance(balanceService)
    editBalance(balanceService)
}