package com.financefox.chat.financefox.transition.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.CategoryService
import kz.rmr.chatmachinist.api.transition.DialogBuilder
import kz.rmr.chatmachinist.model.EventType

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.addCategory(categoryService: CategoryService) {
    transition {
        name = "Adding a category"

        condition {
            from = FinanceFoxState.CATEGORY_CHOICES
            button = FinanceFoxButtonType.ADD_CATEGORY
        }

        then {
            to = FinanceFoxState.ADDING_CATEGORY
        }
    }

    transition {
        name = "Category added"

        condition {
            from = FinanceFoxState.ADDING_CATEGORY
            eventType = EventType.TEXT
        }

        action {
            val categoryName = text!!
            categoryService.addCategory(categoryName, context.userInfo!!)
        }

        then {
            to = FinanceFoxState.CATEGORY_ADDED
        }
    }

    transition {
        name = "Go back from category addition to category list"

        condition {
            from = FinanceFoxState.ADDING_CATEGORY
            button = FinanceFoxButtonType.GO_TO_CATEGORIES
        }

        action {
            context.allCategories = categoryService.findAllCategories(context.userInfo!!)
        }


        then {
            to = FinanceFoxState.CATEGORY_CHOICES
        }
    }

    transition {
        name = "Go back from added category to category adding"

        condition {
            from = FinanceFoxState.CATEGORY_ADDED
            button = FinanceFoxButtonType.GO_TO_ADDING_CATEGORY
        }

        then {
            to = FinanceFoxState.ADDING_CATEGORY
        }
    }

    transition {
        name = "Go back from added category to category choises"

        condition {
            from = FinanceFoxState.CATEGORY_ADDED
            button = FinanceFoxButtonType.GO_TO_CATEGORY_CHOICES
        }

        action {
            context.allCategories = categoryService.findAllCategories(context.userInfo!!)
        }

        then {
            to = FinanceFoxState.CATEGORY_CHOICES
        }
    }
}