package com.financefox.chat.financefox.transition.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.CategoryService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.deleteCategory(categoryService: CategoryService) {
    transition {
        name = "Delete category"

        condition {
            from = FinanceFoxState.CATEGORY_SELECTION
            button = FinanceFoxButtonType.DELETE_CATEGORY
        }

        action {
            context.chosenCategory?.name?.let { categoryService.deleteCategory(it) }
        }

        then {
            to = FinanceFoxState.DELETING_CATEGORY
        }
    }

//    transition {
//        name = "Go back from category options to menu"
//
//        condition {
//            from = FinanceFoxState.DELETING_CATEGORY
//            button = FinanceFoxButtonType.GO_TO_
//        }
//
//        then {
//            to = FinanceFoxState.CATEGORY_CHOICES
//        }
//    }
}