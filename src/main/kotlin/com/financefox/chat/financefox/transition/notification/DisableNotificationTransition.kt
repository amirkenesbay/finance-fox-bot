package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.disableNotification(notificationService: NotificationService) {
    transition {
        name = "Disable a notification"

        condition {
            from = FinanceFoxState.NOTIFICATION_SELECTION
            button = FinanceFoxButtonType.DISABLE_NOTIFICATION
        }

        action {
            notificationService.disableNotification(context.chosenNotification!!.id.toString(), chat.id)
        }

        then {
            to = FinanceFoxState.DISABLING_NOTIFICATION
        }
    }
}