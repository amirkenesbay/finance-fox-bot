package com.financefox.chat.financefox.transition.balance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.BalanceService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.showCreatedBalance(balanceService: BalanceService) {
    transition {
        name = "Show created balance"

        condition {
            from = FinanceFoxState.EDITING_BALANCE
            button = FinanceFoxButtonType.BALANCE
        }

        action {
            context.balance = balanceService.getCommonBalance(user)
        }

        then {
            to = FinanceFoxState.BALANCE_CHOICES
        }
    }

    transition {
        name = "Go back from balance choices to menu"

        condition {
            from = FinanceFoxState.BALANCE_CHOICES
            button = FinanceFoxButtonType.GO_BACK_TO_MENU
        }

        then {
            to = FinanceFoxState.MENU
        }
    }
}