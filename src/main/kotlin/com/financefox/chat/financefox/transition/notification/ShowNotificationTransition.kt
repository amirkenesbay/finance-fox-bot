package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.showNotifications(notificationService: NotificationService) {
    transition {
        name = "Show notifications"

        condition {
            from = FinanceFoxState.MENU
            button = FinanceFoxButtonType.NOTIFICATIONS
        }

        action {
            context.allNotifications = notificationService.findAllNotifications()
        }

        then {
            to = FinanceFoxState.NOTIFICATION_CHOICES
        }
    }

    transition {
        name = "Go back from notification options to menu"

        condition {
            from = FinanceFoxState.NOTIFICATION_CHOICES
            button = FinanceFoxButtonType.GO_BACK_TO_MENU
        }

        then {
            to = FinanceFoxState.MENU
        }
    }
}