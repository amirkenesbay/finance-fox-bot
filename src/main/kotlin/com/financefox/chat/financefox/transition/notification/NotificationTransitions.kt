package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.NotificationService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.notificationTransitions(
    notificationService: NotificationService,
) {
    showNotifications(notificationService)
    selectNotification()
    disableNotification(notificationService)
    enableNotification(notificationService)
    typeNotificationName()
    selectNotificationPeriodicity()
    typeNotificationDateTime()
    typeNotificationDescription(notificationService)
}