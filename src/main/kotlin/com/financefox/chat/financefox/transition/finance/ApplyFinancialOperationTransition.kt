package com.financefox.chat.financefox.transition.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.applyFinancialOperation() {
    transition {
        name = "Go back from applying financial operation to starting financial operation"

        condition {
            from = FinanceFoxState.APPLY_FINANCIAL_OPERATION
            button = FinanceFoxButtonType.GO_BACK_TO_FINANCIAL_OPERATIONS
        }

        then {
            to = FinanceFoxState.START_FINANCIAL_OPERATION
        }
    }
}