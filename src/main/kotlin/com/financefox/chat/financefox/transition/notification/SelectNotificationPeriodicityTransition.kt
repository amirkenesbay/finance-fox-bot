package com.financefox.chat.financefox.transition.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.selectNotificationPeriodicity() {
    transition {
        name = "Select notification periodicity"

        condition {
            from = FinanceFoxState.SELECT_NOTIFICATION_PERIODICITY
            button = FinanceFoxButtonType.NOTIFICATION_PERIODICITY
        }

        action {
            context.notificationRequest?.period = buttonText
        }

        then {
            to = FinanceFoxState.TYPE_NOTIFICATION_DATE_TIME
        }
    }
}