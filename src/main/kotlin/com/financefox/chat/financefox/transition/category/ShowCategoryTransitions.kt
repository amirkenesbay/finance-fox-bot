package com.financefox.chat.financefox.transition.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.domain.model.UserInfo
import com.financefox.service.CategoryService
import kz.rmr.chatmachinist.api.transition.DialogBuilder

fun DialogBuilder<FinanceFoxState, FinanceFoxContext>.showCategories(categoryService: CategoryService) {
    transition {
        name = "Show categories"

        condition {
            from = FinanceFoxState.MENU
            button = FinanceFoxButtonType.CATEGORIES
        }

        action {
//            val user1 = UserInfo(username = "Amer1807", firstName = "Amir", lastName = "Atamekenov", telegramUserId = 319593809L, languageCode = "en")
//            val user2 = UserInfo(username = "FahrieOmerkaan", firstName = "Fahrie", lastName = "Omerkaan", telegramUserId = 502113794L, languageCode = "ru")
//            val users = listOf(user1, user2)
            println("Teeest " + context.userInfo)
            context.allCategories = categoryService.findAllCategories(context.userInfo!!)
        }

        then {
            to = FinanceFoxState.CATEGORY_CHOICES
        }
    }

    transition {
        name = "Go back from category options to menu"

        condition {
            from = FinanceFoxState.CATEGORY_CHOICES
            button = FinanceFoxButtonType.GO_BACK_TO_MENU
        }

        then {
            to = FinanceFoxState.MENU
        }
    }
}