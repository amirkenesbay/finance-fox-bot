package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.disableNotificationReply() {
    reply {
        state = FinanceFoxState.DISABLING_NOTIFICATION

        message {
            newMessage = true
            newPinnedMessage = true
            text = "Уведомление отключено"
        }
    }
}