package com.financefox.chat.financefox.reply.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.selectDateReply() {
    reply {
        state = FinanceFoxState.SELECT_DATE

        message {
            text = "Выбери дату"

            keyboard {
                buttonRow {
                    button {
                        text = "Другая дата"
                        type = FinanceFoxButtonType.FINANCIAL_OPERATION_OTHER_DATE
                    }
                }

                buttonRow {
                    button {
                        text = "Сегодня"
                        type = FinanceFoxButtonType.FINANCIAL_OPERATION_DATE
                    }

                    button {
                        text = "Вчера"
                        type = FinanceFoxButtonType.FINANCIAL_OPERATION_DATE
                    }

                    button {
                        text = "Позавчера"
                        type = FinanceFoxButtonType.FINANCIAL_OPERATION_DATE
                    }
                }

                buttonRow {
                    button {
                        text = "⬅\uFE0F Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_SELECTING_CATEGORY
                    }
                }
            }
        }
    }

    reply {
        state = FinanceFoxState.TYPE_OTHER_DATE

        message {
            text = "Напиши дату по доходу в формате 29/03/2024"

            keyboard {
                buttonRow {
                    button {
                        text = "⬅\uFE0F Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_SELECTING_DATE
                    }
                }
            }
        }
    }
}