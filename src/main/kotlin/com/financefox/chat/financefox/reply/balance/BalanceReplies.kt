package com.financefox.chat.financefox.reply.balance

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.builders.RepliesBuilderImpl

fun RepliesBuilderImpl<FinanceFoxState, FinanceFoxContext>.balanceReplies() {
    createBalanceReply()
    showCreatedBalanceReply()
}