package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.enableNotificationReply() {
    reply {
        state = FinanceFoxState.ENABLING_NOTIFICATION

        message {
            newMessage = true
            newPinnedMessage = true
            text = "Уведомление включено"
        }
    }
}