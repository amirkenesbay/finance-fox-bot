package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.selectNotificationReply() {
    reply {
        state = FinanceFoxState.NOTIFICATION_SELECTION

        // TODO: Update button text to "Включить" after button "Отключить" was processed
        message {
            text = "Выбери действия по этому уведомлению"

            keyboard {
                buttonRow {
                    button {
                        text = "Удалить"
                        type = FinanceFoxButtonType.DELETE_NOTIFICATION
                    }
                    button {
                        text = "Изменить"
                        type = FinanceFoxButtonType.UPDATE_NOTIFICATION
                    }
                    button {
                        text = "Отключить"
                        type = FinanceFoxButtonType.DISABLE_NOTIFICATION
                    }
                    button {
                        text = "Включить"
                        type = FinanceFoxButtonType.ENABLE_NOTIFICATION
                    }
                }
            }
        }
    }
}