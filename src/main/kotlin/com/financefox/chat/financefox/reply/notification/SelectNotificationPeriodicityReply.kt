package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.domain.model.NotificationPeriods.Companion.periodicityList
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.selectNotificationPeriodicityReply() {
    reply {
        state = FinanceFoxState.SELECT_NOTIFICATION_PERIODICITY

        message {
            newMessage = true
            newPinnedMessage = true
            text = "Выбери периодичность уведомлении"

            keyboard {
                periodicityList().forEach { text ->
                    buttonRow {
                        button {
                            this.text = text
                            type = FinanceFoxButtonType.NOTIFICATION_PERIODICITY
                        }
                    }
                }
            }
        }
    }
}