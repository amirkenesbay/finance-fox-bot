package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.typeNotificationDateTimeReply() {
    reply {
        state = FinanceFoxState.TYPE_NOTIFICATION_DATE_TIME

        message {
            text = "Напиши дату и время в формате 30/04/2024 23:12"
        }
    }
}