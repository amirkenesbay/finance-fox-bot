package com.financefox.chat.financefox.reply.balance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.showCreatedBalanceReply() {
    reply {
        state = FinanceFoxState.BALANCE_CHOICES

        message {
            if (context.isBalanceCreated == true) {
                newMessage = true
                text = "Создан баланс с суммой: ${context.balance}"
            } else {
                text = "Сумма на вашем балансе: ${context.balance}"
            }

            keyboard {
                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_MENU
                    }
                }
            }
        }
    }
}