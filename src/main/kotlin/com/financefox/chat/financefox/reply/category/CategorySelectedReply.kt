package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.selectingOptionOnCategoryReply() {
    reply {
        state = FinanceFoxState.CATEGORY_SELECTION

        message {
            // TODO: добавить название выбранной категории
            text = "Выбери опцию по этой категории"// + it.name;

            keyboard {
                buttonRow {
                    button {
                        text = "Удалить"
                        type = FinanceFoxButtonType.DELETE_CATEGORY
                    }
                    button {
                        text = "Изменить"
                        type = FinanceFoxButtonType.UPDATE_CATEGORY
                    }
                }

                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_TO_CATEGORIES
                    }
                }
            }
        }
    }
}