package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.deletingCategoryReply() {
    reply {
        state = FinanceFoxState.DELETING_CATEGORY

        message {
            newMessage = true
            text = "Удалена категория: ${context.chosenCategory?.name}"

//            keyboard {
//                buttonRow {
//                    button {
//                        text = "Назад"
//                        type = FinanceFoxButtonType.GO_TO_CATEGORIES
//                    }
//                }
//            }
        }
    }
}