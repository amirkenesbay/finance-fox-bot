package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.typeNotificationNameReply() {
    reply {
        state = FinanceFoxState.TYPE_NOTIFICATION_NAME

        message {
            text = "Напиши название уведомления"
        }
    }
}