package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.categoryAddedReply() {
    reply {
        state = FinanceFoxState.CATEGORY_ADDED

        message {
            newMessage = true
            text = "Категория добавлена"

            keyboard {
                buttonRow {
                    button {
                        text = "Добавить еще"
                        type = FinanceFoxButtonType.GO_TO_ADDING_CATEGORY
                    }
                }
                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_TO_CATEGORY_CHOICES
                    }
                }
            }
        }
    }
}