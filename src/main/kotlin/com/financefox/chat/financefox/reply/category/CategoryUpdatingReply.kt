package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.updatingCategoryReply() {
    reply {
        state = FinanceFoxState.UPDATING_CATEGORY

        message {
            text = "Напиши новое название категории"

            keyboard {
                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_TO_CATEGORIES
                    }
                }
            }
        }
    }
}