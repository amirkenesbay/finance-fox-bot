package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.categoryUpdatedReply() {
    reply {
        state = FinanceFoxState.CATEGORY_UPDATED

        message {
            newMessage = true
            text = "Категория обновлена"

//            keyboard {
//                buttonRow {
//                    button {
//                        text = "Добавить еще"
//                        type = FinanceFoxButtonType.GO_TO_ADDING_CATEGORY
//                    }
//                }
//            }
        }
    }
}