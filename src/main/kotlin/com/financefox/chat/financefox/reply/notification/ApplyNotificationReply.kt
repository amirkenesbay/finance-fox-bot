package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.applyNotificationReply() {
    reply {
        state = FinanceFoxState.APPLY_NOTIFICATION

        message {
            newMessage = true
            newPinnedMessage = true
            text = "Уведомление успешно добавлено"
        }
    }
}
