package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.addingCategoryReply() {
    reply {
        state = FinanceFoxState.ADDING_CATEGORY

        message {
            text = "Напиши название категории"

            keyboard {
                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_TO_CATEGORIES
                    }
                }
            }
        }
    }
}