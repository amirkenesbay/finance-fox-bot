package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.*
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.notificationChoicesReply() {
    reply {
        state = FinanceFoxState.NOTIFICATION_CHOICES

        message {
            text = "Список уведомлении"

            keyboard {
                this@message.context.allNotifications
                    .groupBy { it.name }
                    .forEach { notification: Map.Entry<String?, List<NotificationRequest>> ->
                        notification.key?.let {
                            buttonRow {
                                button {
                                    text = it
                                    type = FinanceFoxButtonType.NOTIFICATIONS
                                }
                            }
                        }
                    }

                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_MENU
                    }
                    button {
                        text = "Добавить"
                        type = FinanceFoxButtonType.ADD_NOTIFICATION
                    }
                }
            }
        }
    }
}