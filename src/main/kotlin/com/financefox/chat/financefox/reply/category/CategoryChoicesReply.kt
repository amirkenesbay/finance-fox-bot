package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.Category
import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.categoriesReply() {
    reply {
        state = FinanceFoxState.CATEGORY_CHOICES

        message {
            text = "Список категории"

            keyboard {
                this@message.context.allCategories
                    .groupBy { it.name }
                    .forEach { category: Map.Entry<String, List<Category>> ->

                        buttonRow {
                            button {
                                text = category.key
                                type = FinanceFoxButtonType.CATEGORIES
                            }
                        }

                    }

                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_MENU
                    }
                    button {
                        text = "Добавить"
                        type = FinanceFoxButtonType.ADD_CATEGORY
                    }
                }
            }
        }
    }
}