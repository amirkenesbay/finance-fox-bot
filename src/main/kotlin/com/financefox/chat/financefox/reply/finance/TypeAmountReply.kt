package com.financefox.chat.financefox.reply.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.typeAmountReply() {
    reply {
        state = FinanceFoxState.TYPE_AMOUNT

        message {
            val invalidDateMessage = "Неверный формат даты"
            val amountPromptMessage = "Напиши сумму \uD83D\uDCB0"

            text = when {
                context.isInvalidDate!! -> {
                    newMessage = true
                    invalidDateMessage
                }
                context.isChosenOtherDate!! -> {
                    newMessage = true
                    amountPromptMessage
                }
                else -> amountPromptMessage
            }

            keyboard {
                buttonRow {
                    button {
                        text = "↩\uFE0F Попробовать еще раз"
                        type = FinanceFoxButtonType.GO_BACK_TO_TYPING_OTHER_DATE
                    }
                }
            }

            if (!context.isInvalidDate!!) {
                keyboard {
                    buttonRow {
                        button {
                            text = "⬅\uFE0F Назад"
                            type = FinanceFoxButtonType.GO_BACK_TO_SELECTING_DATE
                        }
                    }
                }
            }
        }
    }
}