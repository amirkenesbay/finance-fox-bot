package com.financefox.chat.financefox.reply.menu

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.menuReply() {
    reply {
        state = FinanceFoxState.MENU

        message {
            text = """
                Салем, 👋🏼
                Давай научимся грамотно управлять личными финансами! 💵
                Выбери необходимые пункты ниже 👇🏼
                """.trimIndent()

            keyboard {
                inline = true

                buttonRow {
                    button {
                        text = "Выбрать валюту"
                        type = FinanceFoxButtonType.CURRENCY
                    }
                    button {
                        text = "Уведомления"
                        type = FinanceFoxButtonType.NOTIFICATIONS
                    }
                }

                buttonRow {
                    button {
                        text = "Баланс"
                        type = FinanceFoxButtonType.BALANCE
                    }
                    button {
                        text = "Категория"
                        type = FinanceFoxButtonType.CATEGORIES
                    }
                }
            }
        }
    }
}