package com.financefox.chat.financefox.reply.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.typeDescriptionReply() {
    reply {
        state = FinanceFoxState.TYPE_DESCRIPTION

        message {
            newMessage = true
            text = "Напиши комментарии"

            keyboard {
                buttonRow {
                    button {
                        text = "⏭\uFE0F Пропустить этот шаг"
                        type = FinanceFoxButtonType.SKIP_TYPING_DESCRIPTION
                    }
                }
                buttonRow {
                    button {
                        text = "⬅\uFE0F Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_TYPING_AMOUNT
                    }
                }
            }
        }
    }
}