package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.builders.RepliesBuilderImpl

fun RepliesBuilderImpl<FinanceFoxState, FinanceFoxContext>.notificationReplies() {
    notificationChoicesReply()
    selectNotificationReply()
    disableNotificationReply()
    enableNotificationReply()
    typeNotificationNameReply()
    selectNotificationPeriodicityReply()
    typeNotificationDateTimeReply()
    typeNotificationDescriptionReply()
    applyNotificationReply()
}