package com.financefox.chat.financefox.reply.balance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.createBalanceReply() {
    reply {
        state = FinanceFoxState.EDITING_BALANCE

        message {
            text = "Введи остаток на основном счете"

            keyboard {
                buttonRow {
                    button {
                        text = "Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_MENU
                    }
                }
            }
        }
    }
}