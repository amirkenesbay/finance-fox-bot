package com.financefox.chat.financefox.reply.notification

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.typeNotificationDescriptionReply() {
    reply {
        state = FinanceFoxState.TYPE_NOTIFICATION_DESCRIPTION

        // TODO: add incorrect date reply for TypeNotificationDateTime transition
        message {
            newMessage = true
            newPinnedMessage = true
            text = "Напиши описание уведомлении"

            keyboard {
                buttonRow {
                    button {
                        text = "⏭\uFE0F Пропустить этот шаг"
                        type = FinanceFoxButtonType.SKIP_TYPING_NOTIFICATION_DESCRIPTION
                    }
                }
            }
        }
    }
}