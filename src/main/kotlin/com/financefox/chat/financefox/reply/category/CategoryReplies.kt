package com.financefox.chat.financefox.reply.category

import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.builders.RepliesBuilderImpl

fun RepliesBuilderImpl<FinanceFoxState, FinanceFoxContext>.categoryReplies() {
    categoriesReply()

    selectingOptionOnCategoryReply()

    addingCategoryReply()

    categoryAddedReply()

    deletingCategoryReply()

    updatingCategoryReply()

    categoryUpdatedReply()
}