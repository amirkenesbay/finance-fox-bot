package com.financefox.chat.financefox.reply.finance

import com.financefox.domain.model.Category
import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.selectCategoryReply() {
    reply {
        state = FinanceFoxState.SELECT_CATEGORY

        message {
            text = "Выбери категорию"

            keyboard {
                this@message.context.allCategories
                    .groupBy { it.name }
                    .forEach { category: Map.Entry<String, List<Category>> ->

                        buttonRow {
                            button {
                                text = category.key
                                type = FinanceFoxButtonType.CATEGORIES
                            }
                        }
                    }

                buttonRow {
                    button {
                        text = "⬅\uFE0F Назад"
                        type = FinanceFoxButtonType.GO_BACK_TO_FINANCIAL_OPERATIONS
                    }
                }
            }
        }
    }
}