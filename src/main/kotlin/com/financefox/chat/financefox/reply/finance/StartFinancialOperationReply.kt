package com.financefox.chat.financefox.reply.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.startFinancialOperationReply() {
    reply {
        state = FinanceFoxState.START_FINANCIAL_OPERATION

        message {
            text = """
                Опции по финансовым операциям 💰
                """.trimIndent()

            keyboard {
                inline = true

                buttonRow {
                    button {
                        text = "Доход \uD83D\uDCC8"
                        type = FinanceFoxButtonType.INCOME
                    }
                    button {
                        text = "Расход \uD83D\uDCC9"
                        type = FinanceFoxButtonType.EXPENSE
                    }
                }
            }
        }
    }
}