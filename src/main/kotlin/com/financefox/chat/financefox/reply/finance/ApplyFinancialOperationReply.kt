package com.financefox.chat.financefox.reply.finance

import com.financefox.domain.model.FinanceFoxButtonType
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder

fun RepliesBuilder<FinanceFoxState, FinanceFoxContext>.applyFinancialOperationReply() {
    reply {
        state = FinanceFoxState.APPLY_FINANCIAL_OPERATION

        message {
            if (!context.isSkippedTypingDescriptionForFinance!!) {
                newMessage = true
            }

            text = "Финансовая операция успешно добавлена"

            keyboard {
                buttonRow {
                    button {
                        text = "↩\uFE0F Добавить еще одну операцию"
                        type = FinanceFoxButtonType.GO_BACK_TO_FINANCIAL_OPERATIONS
                    }
                }
            }
        }
    }
}