package com.financefox.chat.financefox.config

import com.financefox.chat.financefox.dialog.financeFoxDialog
import com.financefox.chat.financefox.dialog.financialOperationDialog
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import com.financefox.service.*
import kz.rmr.chatmachinist.api.transition.ChatBuilder
import kz.rmr.chatmachinist.api.transition.chat
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.scheduling.annotation.EnableScheduling

@Configuration
@EnableMongoRepositories(basePackages = ["com.financefox.repository"])
@EnableScheduling
class FinanceFoxChatConfig(
    private val balanceService: BalanceService,
    private val categoryService: CategoryService,
    private val financialTransactionService: FinancialTransactionService,
    private val notificationService: NotificationService,
    private val userInfoService: UserInfoService
) {
    @Bean
    fun financeFoxChat(): ChatBuilder<FinanceFoxState, FinanceFoxContext> =
        chat {
            name = "Finance Fox Chat"

            initialContext {
                FinanceFoxContext()
            }

            commands {
                command {
                    text = "/start"
                    description = "Открыть Меню"
                }
                command {
                    text = "/financial_operations"
                    description = "Внести расход/доход"
                }
            }

            financeFoxDialog(categoryService, balanceService, notificationService)
            financialOperationDialog(categoryService, financialTransactionService)
        }
}