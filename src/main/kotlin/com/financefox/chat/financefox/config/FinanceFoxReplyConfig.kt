package com.financefox.chat.financefox.config

import com.financefox.chat.financefox.reply.balance.balanceReplies
import com.financefox.chat.financefox.reply.category.categoryReplies
import com.financefox.chat.financefox.reply.finance.financialOperationReplies
import com.financefox.chat.financefox.reply.menu.menuReply
import com.financefox.chat.financefox.reply.notification.notificationReplies
import com.financefox.domain.model.FinanceFoxContext
import com.financefox.domain.model.FinanceFoxState
import kz.rmr.chatmachinist.api.reply.RepliesBuilder
import kz.rmr.chatmachinist.api.reply.replies
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FinanceFoxReplyConfig {
    @Bean
    fun replyBuilder(): RepliesBuilder<FinanceFoxState, FinanceFoxContext> {
        return replies {
            chatName = "Finance Fox Chat"

            menuReply()

            notificationReplies()

            balanceReplies()

            categoryReplies()

            financialOperationReplies()
        }
    }
}